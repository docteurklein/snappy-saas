<?php

namespace Knp\Backend;

use GuzzleHttp\Client;
use GuzzleHttp\Post\PostFile;
use Knp\Backend as Base;
use Knp\Input;

class Saas implements Base
{
    private $client;
    private $url;

    public function __construct(Client $client, $url)
    {
        $this->client = $client;
        $this->url = $url;
    }

    public function transform(Input $html)
    {
        $response = $this->client->post($this->url, [
            'body' => [
                'html' => new PostFile('html', $html->getIterator())
            ],
        ]);

        while (!$response->getBody()->eof()) {
            yield $response->getBody()->read(1024);
        }
    }
}
