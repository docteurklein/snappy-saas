<?php

namespace Knp\Backend;

use Knp\Backend;
use Knp\Input;

class ProcOpen implements Backend
{
    private $cmd;

    public function __construct($cmd)
    {
        $this->cmd = $cmd;
    }

    public function transform(Input $html)
    {
        $descriptorspec = [
            0 => ['pipe', 'r'],
            1 => ['pipe', 'w'],
            2 => ['pipe', 'a'],
        ];

        $process = proc_open($this->cmd, $descriptorspec, $pipes);

        foreach ($html as $bytes) {
            fwrite($pipes[0], $bytes);
        }
        fclose($pipes[0]);

        $hasStdOut = false;
        while ($bytes = fread($pipes[1], 1024)) {
            $hasStdOut = true;
            yield $bytes;
        }
        fclose($pipes[1]);

        $stderr = stream_get_contents($pipes[2]);
        fclose($pipes[2]);
        if (!empty($sterr)) {
            throw new \Exception($stderr);
        }

        if (!$hasStdOut) {
            throw new \Exception("{$this->cmd} returned an empty stdout.");
        }

        proc_close($process);
    }
}
