<?php

namespace Knp;

class Input implements \IteratorAggregate
{
    private $bytes;

    private function __construct(\Iterator $bytes)
    {
        $this->bytes = $bytes;
    }

    public static function create($resource)
    {
        if ($resource === '-') {
            return self::fromStdin();
        }
        if (is_file($resource)) {
            return self::fromFile($resource);
        }

        return self::fromBytes($resource);
    }

    public function getIterator()
    {
        return $this->bytes;
    }

    public static function fromFile($file)
    {
        return new self(call_user_func(function($file) {
            $fd = fopen($file, 'r');
            while ($chunk = fread($fd, 1024)) {
                yield $chunk;
            }
            fclose($fd);
        }, $file));
    }

    public static function fromStdin()
    {
        return self::fromFile('php://stdin');
    }

    public static function fromBytes($bytes)
    {
        return new self(call_user_func(function($bytes) {
            foreach (str_split($bytes) as $chunk) {
                yield $chunk;
            }
        }, $bytes));
    }
}
