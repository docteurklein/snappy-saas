<?php

namespace Knp;

class Pdf implements \IteratorAggregate
{
    private $content;

    public static function fromBytes(\Iterator $bytes)
    {
        return new self($bytes);
    }

    private function __construct($content)
    {
        $this->content = $content;
    }

    public function toFile($path)
    {
        $fd = fopen($path, 'w+');
        foreach ($this->content as $chunk) {
            fwrite($fd, $chunk);
        }

        fclose($fd);
    }

    public function getIterator()
    {
        return $this->content;
    }
}
