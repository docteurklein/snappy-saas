<?php

namespace Knp;

class Container
{
    private $config = [];

    public function __construct(array $config = [])
    {
        $this->config = $config;
    }

    public function __invoke($id)
    {
        if (!array_key_exists($id, $this->config)) {
            throw new \InvalidArgumentException("$id DIC config is required.");
        }

        if (is_callable($this->config[$id])) {
            return call_user_func($this->config[$id], $this);
        }

        return $this->resolveValue($this->config[$id]);
    }

    public function resolveValue($value)
    {
        return preg_replace_callback('/%%|%([^%\s]+)%/', function ($match) {
            return $this($match[1]);
        }, $value);
    }
}
