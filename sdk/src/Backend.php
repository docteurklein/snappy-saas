<?php

namespace Knp;

interface Backend
{
    public function transform(Input $html);
}
