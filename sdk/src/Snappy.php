<?php

namespace Knp;

use Knp\Pdf;
use Knp\Input;

class Snappy
{
    private $backend;

    public function __construct(Backend $backend)
    {
        $this->backend = $backend;
    }

    public function transform($html)
    {
        $input = Input::create($html);

        $pdf = $this->backend->transform($input);

        return Pdf::fromBytes($pdf);
    }
}
