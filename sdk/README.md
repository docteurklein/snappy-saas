Snappy SDK
----------

# What ?

A simple php5.5+ library that handles html to pdf transformation.

# Why ?

Current Snappy version currently only handles wkhtmltopdf. This approach will enable to switch between different backends,
be it wkhtmltopdf, weasyprint or a remote backend (like snappy-saas).


# How ?

    ./bin/snappy \
        'http://localhost:8000/app_dev.php/api/convert/wkhtmltopdf?apiKey=kljuc' \ # snappy-saas url
        '<b>test</b> yep' \ # input
        test.pdf # output


Input can either be an html string, a path to a local html file, or `-`, in which case it will be read from stdtin.
