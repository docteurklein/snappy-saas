snappy-saas
-----------

# What

A web service to transform html to pdf.

# Why

Avoid local installations of wkhtmltopdf, which never works. Also, multiple-backends (weasyprint, wkhtmltopdf, ...).

# How

    fig up -d

    curl \
        -X POST \
        -F "html=@fixtures/simple.html" \
        "http://localhost:8000/app_dev.php/api/convert/wkhtmltopdf?apiKey=test" > simple.pdf

    curl \
        -X POST \
        --data "url=http://github.com" \
        "http://localhost:8000/app_dev.php/api/convert/wkhtmltopdf?apiKey=test" > github.pdf


