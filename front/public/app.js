"use strict";

angular
    .module('snappyApp', [
        'ui.router',
        'ngResource',
        'register'
    ])

    .config(function ($resourceProvider, $httpProvider, $stateProvider, $urlRouterProvider) {
        // Don't strip trailing slashes from calculated URLs
        $resourceProvider.defaults.stripTrailingSlashes = false;

        $urlRouterProvider
            .otherwise('/');

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: '/views/home.html'
            })
            .state('about', {
                url: '/about',
                templateUrl: '/views/about.html'
            });
    });
