"use strict";

var register = angular.module('register');

register
    .service('ApplicationAPI', function ($resource) {
        var resource = $resource('/backend/public/api/application/', {}, {
            create: { method:'POST' }
        });

        this.create = function (application) {
            return resource.create(application).$promise;
        };
    });
