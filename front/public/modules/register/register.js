"use strict";

angular
    .module('register', [])

    .config(function ($stateProvider) {
        $stateProvider
            .state('register', {
                url: '/register',
                templateUrl: '/modules/register/register.html',
                controller: function($scope, $state, ApplicationAPI) {
                    $scope.errors = [];
                    $scope.application = {
                        name: null,
                        user: {
                            email: null,
                            username: null,
                            plainPassword: null
                        }
                    };

                    $scope.submit = function(application) {
                        ApplicationAPI.create(application)
                            .then(function(response) {
                                $state.go('register_success', {
                                    apiKey: response.apiKey
                                });
                            })
                            .catch(function(response) {
                                $scope.errors = response.data;
                            });
                    };
                }
            })
            .state('register_success', {
                url: '/register/success',
                params: { apiKey: null },
                templateUrl: '/modules/register/success.html',
                controller: function($scope, $stateParams) {
                    $scope.apiKey = $stateParams.apiKey;
                }
            });
    });
