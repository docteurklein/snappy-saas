<?php
namespace App\Controller;

use App\Entity\App;

class AppsController extends Controller
{
    public function indexAction()
    {
        return ['apps' => $this->get('app.entity.app_repository')->findByUser($this->getUser())];
    }

    public function newAction()
    {
        $app = new App();
        $app->setUser($this->getUser());
        $form = $this->createBoundObjectForm($app);
        if ($form->isBound() && $form->isValid()) {
            $app->setApiKey(md5(time()));
            $this->persist($app);
            $this->flush();
            $this->addFlash('success');

            return $this->redirectToRoute('app_apps_index');
        }

        return ['form' => $form->createView()];
    }
}
