<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ApiController extends Controller
{
    public function quotaAction(Request $request)
    {
        $key = sprintf('app:%s', $request->get('apiKey'));
        die(var_dump($key));
        $redis = $this->get('snc_redis.default');
        if ($redis->get($key) <= 100) {
            $redis->incr($key);
            return new Response();
        }
        throw $this->createAccessDeniedException(sprintf('Quota for api key %s exceeded', $request->get('apiKey')));
    }
}
