<?php
namespace App\Controller;

use App\Entity\App;
use App\Form\Type\RegisterType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PublicApiController extends Controller
{
    public function applicationCreateAction(Request $request)
    {
        $app = new App();

        $form = $this->get('form.factory')->createNamed('', new RegisterType(), $app, [
            'csrf_protection' => false,
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($app->getUser());

            $app->setApiKey(md5(time()));

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($app);
            $manager->flush();

            return new JsonResponse([
                'apiKey' => $app->getApiKey(),
            ], 201);
        }

        return $form;
    }
}
