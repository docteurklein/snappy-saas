<?php
namespace App\Security;

use App\Entity\App;
use App\Entity\AppRepository;
use App\Entity\UserRepository;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class ApiKeyUserProvider implements UserProviderInterface
{
    public function __construct(AppRepository $appRepo, UserRepository $userRepo)
    {
        $this->appRepo = $appRepo;
        $this->userRepo = $userRepo;
    }

    public function getUsernameForApiKey($apiKey)
    {
        /**
         * @var App $app
         */
        $app = $this->appRepo->findOneBy(['apiKey' => $apiKey]);
        if (null === $app) {
            throw new AccessDeniedHttpException('App with such API key was not found');
        }

        return $app->getUser()->getUsername();
    }

    public function loadUserByUsername($username)
    {
        return $this->userRepo->findOneBy(['username' => $username]);
    }

    public function refreshUser(UserInterface $user)
    {
// this is used for storing authentication in the session
// but in this example, the token is sent in each request,
// so authentication can be stateless. Throwing this exception
// is proper to make things stateless
        throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return 'Symfony\Component\Security\Core\User\User' === $class;
    }
}
