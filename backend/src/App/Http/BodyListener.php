<?php

namespace App\Http;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class BodyListener
{
    public function populateRequestFromJsonBody(GetResponseEvent $event)
    {
        if (!in_array($event->getRequest()->getMethod(), ['POST', 'PUT', 'PATCH'])) {
            return;
        }

        $data = json_decode($event->getRequest()->getContent(), true);

        if (is_array($data)) {
            $event->getRequest()->request->replace($data);
        }
    }
}
