<?php

namespace App\Http;

use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;

class ViewListener
{
    public function transformFormErrors(GetResponseForControllerResultEvent $event)
    {
        $result = $event->getControllerResult();

        if ($result instanceof FormInterface && !$result->isValid()) {
            $event->setResponse($this->generateResponseFromFormErrors($result));
        }
    }

    protected function generateResponseFromFormErrors(FormInterface $form)
    {
        $exposeErrors = function ($formError) use (&$exposeErrors) {
            if ($formError instanceof FormError) {
                return $formError->getMessage();
            }

            return [
                $formError->getForm()->getName() => array_map($exposeErrors, iterator_to_array($formError))
            ];
        };

        return new JsonResponse(
            array_map($exposeErrors, iterator_to_array($form->getErrors(true, false))),
            400
        );
    }
}
