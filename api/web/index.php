<?php

require __DIR__.'/../vendor/autoload.php';

$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();

$client = new GearmanClient;
$client->addServer('gearman', 4730);

$checker = new Knp\SnappySass\Api\Client(new Buzz\Client\Curl, 'http://backend:8000');

if (!$checker->hasRight($request->query->get('apiKey'))) {
    throw new \Exception('invalid key.');
}

$contents = file_get_contents($request->files->get('html')->getPathname());

$args = sprintf('%s /tmp/%s.pdf', base64_encode($contents), time());

$pdf = $client->doNormal(
    $request->query->get('backend'),
    $args
);

$response = new \Symfony\Component\HttpFoundation\Response($pdf, 200, ['Content-Type' => 'application/pdf']);
$response->send();
