<?php

namespace Knp\SnappySass\Api;

use Buzz\Client\ClientInterface;
use Buzz\Message\Response;
use Buzz\Message\Request;

class Client
{
    private $client;
    private $host;

    public function __construct(ClientInterface $client, $host)
    {
        $this->client = $client;
        $this->host = $host;
    }

    public function hasRight($token)
    {
        $request = new Request('HEAD', sprintf('/app_dev.php/backend/api/quota?apiKey=%s', $token), $this->host);
        $response = new Response;

        $this->client->send($request, $response);

        return $response->getStatusCode() === 200;
    }
}
